package com.cathayholdings.profiles;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootDemoProfilesApplication {

	@Value("${demo.profile.string}") // @Value: 從配置文件讀取值。
	private String profile;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemoProfilesApplication.class, args);
	}

	@GetMapping("/profile")
	public String profile(){
		return profile; // 返回從配置文件中的Profile文字
	}
}
