package com.cathayholdings.beans.dependencyinjection.service;

import org.springframework.stereotype.Service;

@Service // 跟Spring告知要管理這個Class
public class DemoService {

    public String say(String injectType){
        return String.format("DemoService注入方式為: %s", injectType);
    }
}
