package com.cathayholdings.springbootdemoyaml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootDemoYamlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoYamlApplication.class, args);
    }

    public String hello() {
        return "Hello World!! This is YAML";
    }

}
