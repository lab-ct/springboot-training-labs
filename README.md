# Spring Boot 入門實驗室

## 章節

### Day1

### Getting Started With Spring
1. Why Spring?
2. Understanding the Full Spring Ecosystem
3. Introducing Spring Boot 2

### Development Environment
1. Installing Java on Windows
2. Maven on Windows
2. Gradle on Windows
3. Installing STS on Windows

### Spring Boot Fundamentals
1. Spring Initializer
2. Hello, Spring Boot!
3. Application Properties & YAML Configuration
4. Profiles
5. Spring Beans & Dependency Injection
6. Spring AOP

### Data Access with Spring
1. Creating an Application using H2's in-memory database
2. Implementing findById Spring JDBC Query Method
3. Implementing deleteById Spring JDBC Update Method
4. Implementing insert and update Spring JDBC Update Methods
5. Creating a custom Spring JDBC RowMapper

### Day2

### Data Access with JPA
1. JPA Intro2.Defining Person Entity
3. Implementing findById JPA Repository Method
4. Implementing insert and update JPA Repository Methods
5. Implementing deleteById JPA Repository Method
6. Implementing pagination use JPA Repository

### Transaction Management
1. Using @Transactional
2. Rolling back

### Building REST APIs
1. REST API Intro
2. HTTP Request Methods (Verbs)
3. HTTP Status Codes
4. Content Negotiation
5. REST Template

### CRUD
1. Read
2. Create
3. Validation
4. Update
5. Delete

### Spring Cloud Stream
1. Spring Cloud Stream Intro
2. RabbitMQ Intro
3. 3.Retry

### Other
1. Project Lombok
2. Swagger
3. Spring Boot Actuator
4. Use Docker
5. 三層式架構

