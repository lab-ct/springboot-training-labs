# Application Properties & YAML Configuration

>1. Spring Boot有自己的預設內容(ex: port 8080)，若要修改預設內容。<br>
>Spring Boot提供了application.properties或者application.yml(yaml)兩種配置檔格式供修改
>2. 此範例有兩個專案一個是使用Properties檔作為Spring Boot專案配置檔。另一專案使用YAML檔案作為配置檔

## spring-boot-demo-properties: application.properties

```properties
server.port=8090
server.address= 127.0.0.1
server.max-http-header-size=8KB

server.servlet.session.timeout=30
server.servlet.session.persistent=false

server.tomcat.max-threads=200
server.tomcat.max-connections=8192
server.tomcat.uri-encoding=UTF-8
server.tomcat.max-http-post-size=2MB
server.tomcat.accesslog.enabled=false
```
## spring-boot-demo-properties: application.yml

```yaml
server:
  port: 8090
  address: 127.0.0.1
  max-http-header-size: 8KB
  servlet:
    session:
      timeout: 30
      persistent: false
  tomcat:
    max-threads: 200
    max-connections: 8192
    uri-encoding: UTF-8
    max-swallow-size: 2MB
    accesslog:
      enabled: false
```

## properties、YAML檔案格式差異與特性
> 從以上兩個配置檔內容可以看出以下幾點

1. YAML有天然的樹狀結構
2. properties文件中是以"."連接，通過"="賦值。<br>
   YAML文件中是用":"作為分層，通過": "賦值(冒號加上一個空白)
3. YAML檔案的縮進不能使用TAB鍵，縮進為兩個空白

### 結論

* 從以上兩個配飾檔可以看出Porperties跟YAML的格式差異
* YAML相較於properties較為簡潔
* Spring Boot官方推薦使用YAML檔進行配置