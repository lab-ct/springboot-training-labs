package com.cathayholdings.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring Boot啟動類
 */
@SpringBootApplication // Spring Boot 核心註解，主要目的為開啟自動配置
@RestController // 構建REST web service
public class SpringBootDemoHelloWorldApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoHelloWorldApplication.class, args);
    }

    @GetMapping("/hello") // 使用HTTP Method: GET，Url路徑: hello。來獲取資源
    public String helloWorld() {
        return "Hello World From Spring Boot!!";
    }
}
