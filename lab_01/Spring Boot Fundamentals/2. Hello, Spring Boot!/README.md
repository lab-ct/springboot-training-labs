# Hello, Spring Boot!

> 此範例為使用spring boot建立一個簡單web service回傳hello world字串

## Prepare

 
1. 至[Spring Initializr](https://start.spring.io/)生成專案

    * 專案所選取內容如下:

    <table>
        <tr>
            <th>attribute</th>
            <th>context</th>
        </tr>
        <tr align="center">
            <td>Project</td>
            <td>Gradle Project</td>
        </tr>
        <tr align="center">
            <td>Language</td>
            <td>Java</td>
        </tr>
        <tr align="center">
            <td>Spring Boot</td>
            <td>2.2.5</td>
        </tr>
        <tr align="center">
            <td>Project Metadata</td>
        <td>
            Group: com.cathayholdings<br>
            Artifact: spring-boot-demo-helloworld 
        </td>
        </tr>
        <tr align="center" >
            <td>Depencies</td>
            <td>Spring Web</td>
        </tr>
    </table>

    ![Imgur Image](https://i.imgur.com/iPv33jZ.png)
 
2. 按下Genernate下載專案且解壓縮，再將專案資料夾匯入IDE中

## SpringBootDemoHelloworldApplication.java
```java
/**
 * Spring Boot啟動類
 */
@SpringBootApplication // Spring Boot 核心註解，主要目的為開啟自動配置
@RestController // 構建REST web service
public class SpringBootDemoHelloWorldApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoHelloWorldApplication.class, args);
    }

    @GetMapping("/hello") // 使用HTTP Method: GET，Url路徑: hello。來獲取資源
    public String helloWorld() {
        return "Hello World From Spring Boot!!";
    }
}
```

## Result

Spring boot啟動後，在瀏覽器上輸入網址:[http://localhost:8080/hello](http://localhost:8080/hello)<br/>看到以下畫面代表成功啟動服務
![Imgur image](https://i.imgur.com/OmJGC8y.png)



