# Spring Beans & Dependency Injection

### Spring Bean是甚麼
1. 定義
   <br>
   * Spring官方對Bean的解釋
   > In Spring, the objects that form the backbone of your application and that are managed by the Spring IoC container are called beans. A bean is an object that is instantiated, assembled, and otherwise managed by a Spring IoC container.
   
   * 翻譯
   > 在Spring中，構成應用程序主幹並由Spring IoC容器管理的對象稱為Bean。 Bean是由Spring IoC容器實例化，組裝和以其他方式管理的物件。
   
   * 簡單來說
     1. Bean是物件，一個或多個不限定
     2. Bean是由Spring中一個叫IoC的東西管理
     3. 我們的Spring Boot專案是由一個個Bean構成
     
     1跟3都好理解，那IoC又是甚麼東西?
2. IoC(控制反轉)與DI(依賴注入)
   * IoC - Inversion of Control 控制反轉
     * 將控制權交到Spring手上，讓Spring管理程式中的Bean
   * DI - Dependency Injection 依賴注入
     * 實現IoC的手段之一，讓Spring能夠自動創建所需的Bean，並注入到需要這些Bean的地方

### Bean的註解

1. Bean的聲明
   - @Component: 組件。沒有特殊意義
   - @Service: Service層使用(業務邏輯)
   - @Controller
   - @Repository: 儲存庫，DAO層使用
   - ...
   > 以上這幾種註解，目的都只是告訴Spring要管理這些類別。
     因此不管使用哪一個Annotation，結果都是交由Spring管理，
   > 只是語意不同而已
   
2. 注入Bean(Dependency Injection)
   - **@Autowired**：Spring提供的註解，讓Spring自動注入
   
   * 注入方式有分成以下幾種
      1. Field
      2. Setter
      3. Constructor
   
3. 範例程式

DemoService.java
```java
@Service // 跟Spring告知要管理這個Class
public class DemoService {

    public String say(String injectType){
        return String.format("DemoService注入方式為: %s", injectType);
    }
}
```   

Field方式注入- FieldController.java
```java
@RestController
public class FieldController {

    @Autowired // 把DemoService實體注入到這個Class
    private DemoService demoService;

    @GetMapping("field")
    public String injectType(){
        return demoService.say("field");
    }
}
```
Setter方式注入- SetterController.java
```java
@RestController // 跟Spring告知要管理這個Class
public class SetterController {

    // 需注入的物件
    private DemoService demoService;

    @Autowired // 被注入物件的Setter方法
    public void setDemoService(DemoService demoService){
       this.demoService = demoService;
    }

    @GetMapping("setter")
    public String injectType() {
        return demoService.say("Setter");
    }
}
```

constructor方式注入- ConstructorController.java
```java
@RestController // 跟Spring告知要管理這個Class
public class ConstructorController {

    // 需注入物件
    private DemoService demoService;

    // constructor方式注入物件
    public ConstructorController(DemoService demoService) {
        this.demoService = demoService;
    }

    @GetMapping("constructor")
    public String injectType() {
        return demoService.say("Constructor");
    }
}
```
範例結果

* Field方式注入- [http://localhost:8080/filed](http://localhost:8080/filed)
<br>
![Imgur image](https://imgur.com/DnSOPvN.png)

* Setter方式注入- [http://localhost:8080/setter](http://localhost:8080/setter)
<br>
![Imgur image](https://imgur.com/atLAypA.png)

* Constructor方式注入[http://localhost:8080/constructor](http://localhost:8080/constructor)
<br>
![Imgur image](https://imgur.com/b53Bd13.png)