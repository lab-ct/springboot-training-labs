package com.cathayholdings.beans.dependencyinjection.controller;

import com.cathayholdings.beans.dependencyinjection.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FieldController {

    @Autowired // 把DemoService實體注入到這個Class
    private DemoService demoService;

    @GetMapping("field")
    public String injectType(){
        return demoService.say("field");
    }
}
