package com.cathayholdings.aop.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AopDemoController {

    @GetMapping("aop")
    public String aopDemo(){
        System.out.println("This is /aop EndPoint");
        return "This AOP Return Message";
    }
}
